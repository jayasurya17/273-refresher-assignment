"use strict"

const fetch = require("node-fetch");

// Base URL from which the list of recipes needs to be fetched
var URL = "http://www.recipepuppy.com/api/?";
// List of ingredients that you need in the meal you want to prepare
var IngredientsRequired = ["olives", "onion", "capsicum"];
// Type of meal that you want to prepare
var TypeOfMeal = "pizza";
// The complete URL from which the list of recipes needs to be fetched
URL = URL + "i=" + IngredientsRequired + "&q=" + TypeOfMeal;

/*
Summary.
This function prints the names of the meal along with links to view recipe for the meal and all the ingredients required to prepare it.

@param      { JSON }    JSONResponse     JSON response which was returned from HTTP request.
*/
var PrintLinkAndIngredientsFromJSON = (JSONResponse) => {
    var Results, Name, Link, Ingredients, val;
    Results = JSONResponse["results"];
    console.log("Values taken from Recipe Puppy (http:\/\/www.recipepuppy.com\)");
    console.log("Number of recipes found to prepare " + TypeOfMeal + " with " + IngredientsRequired + " : " + Results.length + "\n\n");
    for (val in Results) {
        Name = Results[val]["title"].replace("/[^a-zA-Z]/","");
        Link = Results[val]["href"];
        Ingredients = Results[val]["ingredients"];
        console.log("Name of meal : " + Name);
        console.log("Link to view recipe : " + Link);
        console.log("All ingredients required : " + Ingredients + "\n");
    }
}

/*
Summary.
This function fetches the values from the URL and prints the values returned from it.
*/
var FetchAndPrint = () => {    
    fetch(URL).then(function(ResponseFromURL) {
        return ResponseFromURL.json();
    }).then(function(JSONFromURL) {
        PrintLinkAndIngredientsFromJSON(JSONFromURL);
    });
}

FetchAndPrint();