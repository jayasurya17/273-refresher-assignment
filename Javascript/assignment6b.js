"use strict"

/*
Summary.
This function converts feet and inches to meters.

@param     { Number }      feet         Feet part of the height of a person
@param     { Number }      incehs       Inches part of the height of a person
@return    { Numer }       Meters       Value converted to meters
*/
var FeetToMeters = (feet, inches) => {
    var Meters = 0;
    Meters = (feet + (inches / 12)) * 0.3048;
    return Meters;
}

/*
Summary.
This function finds BMI of a person. This is calculated by dividing the weight of the 
person in KG by sqaure of the height of the person in meteres. 

@param     { JSON }      ObjectName         Name, age, height and weight of a person
@return    { Promise }       
*/
var FindBMI = (ObjectName) => {
    return new Promise((Resolve, Reject) => {
        setTimeout(() => {
            var BMI, Weight, Height;
            // console.log(ObjectName);
            Weight = ObjectName.Weight;
            Height = FeetToMeters(ObjectName.Height.Feet, ObjectName.Height.Inches);
            BMI =  Weight / (Height * Height);
            console.log("BMI of", ObjectName.Name, "is", BMI.toFixed(1));
            Resolve();
        }, 1000);
    });

}

/*
Summary.
This function first finds BMI of all the 3 at the start, then inside the block, again when the block ended, 
then after modifying a single property of each of the 3 people and finally after reinitialising var type of person.
*/
async function Main () {
    var VarPerson = {
        Name : "VarPerson outside the block",
        Age : 20,
        Weight : 70,
        Height : {
            Feet : 5,
            Inches : 7
        }
    };
    let LetPerson = {
        Name : "LetPerson outside the block",
        Age : 30,
        Weight : 74,
        Height : {
            Feet : 5,
            Inches : 10
        }
    };
    const ConstPerson = {
        Name : "ConstPerson outside the block",
        Age : 25,
        Weight : 60,
        Height : {
            Feet : 6,
            Inches : 0
        }
    };

    console.log("\nCalculating values at the start");
    await FindBMI(VarPerson);
    await FindBMI(LetPerson);
    await FindBMI(ConstPerson);

    {
        var VarPerson = {
            Name : "VarPerson inside the block",
            Age : 20,
            Weight : 70,
            Height : {
                Feet : 5,
                Inches : 7
            }
        };
        let LetPerson = {
            Name : "LetPerson inside the block",
            Age : 30,
            Weight : 74,
            Height : {
                Feet : 5,
                Inches : 10
            }
        };
        const ConstPerson = {
            Name : "ConstPerson inside the block",
            Age : 25,
            Weight : 60,
            Height : {
                Feet : 6,
                Inches : 0
            }
        };

        console.log("\nCalculating values inside the block");
        await FindBMI(VarPerson);
        await FindBMI(LetPerson);
        await FindBMI(ConstPerson);
    }

    console.log("\nCalculating values outside the block");
    await FindBMI(VarPerson);
    await FindBMI(LetPerson);
    await FindBMI(ConstPerson);
    
    VarPerson.Name = "VarPerson after modifying name property";
    LetPerson.Name = "LetPerson after modifying name property";
    ConstPerson.Name = "ConstPerson after modifying name property";

    console.log("\nCalculating values after modifying a single property of the object");
    await FindBMI(VarPerson);
    await FindBMI(LetPerson);
    await FindBMI(ConstPerson);

    var VarPerson = {
        Name : "VarPerson reinitialised",
        Age : 20,
        Weight : 70,
        Height : {
            Feet : 5,
            Inches : 7
        }
    };
    console.log("\nCalculating var type of person value after reinitialising");
    await FindBMI(VarPerson);
}

Main();