"use strict"

// The number of pages written in each chapter by the four authors
var Author1 = { Chapter1 : 250 };
var Author2 = { Chapter2 : 300 };
var Author3 = { Chapter1 : 100, Chapter2 : 215 };
var Author4 = { Chapter2 : 100, Chapter3 : 75 };
// Total number of pages that each chapter has in the book
var TotalNumberOfPagesInEachChapter;
/*
Summary.
Book class which holds the name of the book.
*/
class Book {
    constructor (BookName) {
        this.BookName = BookName;
    }

    /*
    Description.
    This funtion returns the name of the book.

    @return     { String }      BookName        Name of the book
    */
    GetBookName () {
        return this.BookName;
    }
}

/*
Summary.
Pages class which holds the number of pages in a book. This is extended from Book class.
*/
class Pages extends Book {
    constructor (Book, NumberOfPages) {
        super(Book);
        this.NumberOfPages = NumberOfPages; 
    }
    
    /*
    Description.
    This funtion returns the number of pages in the book.

    @return     { Number }      NumberOfPages        Number of pages in the book
    */
    GetNumberOfPages () {
        return this.NumberOfPages;
    }

    /*
    Description.
    This funtion returns the details of the book.

    @return     { String }     Details of the book (Name of the book and number of pages it has)
    */
    DisplayDetails () {
        return this.GetBookName() + " book contains a total of " + this.GetNumberOfPages() + " pages.";
    }
}

/*
Summary.
This funtion consolidates the number of pages in each chapter of the book.

@return     { Promise }
*/
var ConsolidateValues = () => {
    return new Promise(( Resolve, Reject) => {
        setTimeout(() => {
            TotalNumberOfPagesInEachChapter = Object.assign( {}, Author1, Author2, Author3, Author4);
            console.log("Total Number of pages in each chapter is : " + JSON.stringify(TotalNumberOfPagesInEachChapter));
            Resolve();
        }, 1000);
    });

} 

/*
Summary.
This funtion prints the total number of pages in the book.

@return     { Promise }
*/
var PrintDetails = () => {
    return new Promise(( Resolve, Reject ) => {
        setTimeout(() => {
            var TotalNumberOfPages = 0, Chapter;
            for (Chapter in TotalNumberOfPagesInEachChapter) {
                TotalNumberOfPages += TotalNumberOfPagesInEachChapter[Chapter];
            }

            var Book1 = new Pages("Javascript", TotalNumberOfPages);
            console.log(Book1.DisplayDetails());
            Resolve();
        }, 2000);
    });
}

/*
Summary.
This is an asyncronous function which first consolidates the values and then prints the total.
*/
async function Main() {
    await ConsolidateValues();
    await PrintDetails();
}

Main();