"use strict"

/*
Summary.
This function converts liters to gallon.

@param      { Number }      Liters      The number of liters of water a tank can hold
@return     { Number }                  The number of liters of water converted to gallon
*/
var LiterToGallon = (Liters) => {
    return Liters * 0.264172;
}

/*
Summary.
This function finds the number of liters the tank can hold based on it's volume.

@param      { Number }      Volume      Volume of the tank
@return     { Number }                  The number of liters of water the tank can hold
*/
var CubicMetersToLiters = (Volume) => {
    return Volume * 1000;
}

/*
Summary.
This function prints the number of gallons of water a tank can dold.

@param      { Number }      Value      The number of gallons of water the tank can hold
*/
var PrintValue = (Value) => {
    console.log("The tank can hold " + Value + " gallons of water");
}

/*
Summary.
This function prints the number of gallons of water a tank can hold.

@param      { JSON }          Tank          The JSON value with the radius and height of the tank
@param      { Function }      Callback      The callback function which can be called at the end of the function execution
@return     { Promise } 
*/
var WaterHoldingCapacity = (Tank, Callback) => {
    return new Promise (() => {
        setTimeout(() => {
            var Volume = 3.14 * Tank.Radius * Tank.Radius * Tank.Height;
            Callback (LiterToGallon (CubicMetersToLiters (Volume)));
        }, 2000);
    });
}

/*
Summary.
This is an async function which calculates the gallons of water a tank can hold.
*/
async function Main() {
    var Tank1 = {
        Radius : 5,
        Height : 10
    };
    await WaterHoldingCapacity(Tank1, PrintValue);
}

Main();