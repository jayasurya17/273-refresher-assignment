"use strict"

class Player {
    constructor (PlayerName) {
        this.PlayerName = PlayerName;
    }

    static PlayerCount () {
        Count++;
    }

    static SetValue (PlayerValue) {
        this.PlayerValue = PlayerValue;
    }

    static GetValue () {
        return this.PlayerValue;
    }
};

var CalculateScoreWithRegEx = (Value) => {
    var TwoAlphabets, TwoNumbers, SingleAlphabet, SingleNumber, TotalPoints, MatchedValuePos;

    TwoAlphabets = Value.match(/[A-Za-z]{2}/g);
    TwoNumbers = Value.match(/[0-9]{2}/g);
    for (MatchedValuePos in TwoAlphabets) {
        Value = Value.replace(TwoAlphabets[MatchedValuePos], "");
    }
    for (MatchedValuePos in TwoNumbers) {
        Value = Value.replace(TwoNumbers[MatchedValuePos], "");
    }

    SingleAlphabet = Value.match(/[A-Za-z]/g);
    SingleNumber = Value.match(/[0-9]/g);
    for (MatchedValuePos in SingleAlphabet) {
        Value = Value.replace(SingleAlphabet[MatchedValuePos], "");
    }
    for (MatchedValuePos in SingleNumber) {
        Value = Value.replace(SingleNumber[MatchedValuePos], "");
    }

    TotalPoints = (TwoAlphabets.length * 7) + (TwoNumbers.length * 5) + (SingleAlphabet.length * 3) + (SingleNumber.length * 2) + (Value.length);

    return TotalPoints;
}

var IsNumberASCII = (ASCIICode) => {
    if (ASCIICode >= 48 && ASCIICode <= 57){
        return true;
    }
    return false;
}

var IsCharASCII = (ASCIICode) => {
    if (ASCIICode >= 65 && ASCIICode <= 90) {
        return true;
    } else if (ASCIICode >= 97 && ASCIICode <= 122) {
        return true;
    }
    return false;
}

var CalculateScoreWithoutRegEx = (Value) => {
    var TwoAlphabets = 0, TwoNumbers = 0, SingleAlphabet = 0, SingleNumber = 0, TotalPoints = 0, MatchedValuePos = 0;
    var CharCodeOfPreviousElement, CharCodeOfCurrentElement, Pos;
    CharCodeOfPreviousElement = Value.charCodeAt(0);
    for (Pos = 1; Pos < Value.length; Pos++) {  
        CharCodeOfCurrentElement = Value.charCodeAt(Pos);
        if (CharCodeOfPreviousElement == null){
            CharCodeOfPreviousElement = CharCodeOfCurrentElement;
        } else {
            if (IsCharASCII(CharCodeOfPreviousElement) && IsCharASCII(CharCodeOfCurrentElement)) {
                TotalPoints += 7;
                CharCodeOfPreviousElement = null;
            } else if (IsNumberASCII(CharCodeOfPreviousElement) && IsNumberASCII(CharCodeOfCurrentElement)) {
                TotalPoints += 5;
                CharCodeOfPreviousElement = null;
            } else if (IsCharASCII(CharCodeOfPreviousElement)) {
                TotalPoints += 3;
                CharCodeOfPreviousElement = CharCodeOfCurrentElement;
            } else if (IsNumberASCII(CharCodeOfPreviousElement)) {
                TotalPoints += 2;
                CharCodeOfPreviousElement = CharCodeOfCurrentElement;
            } else {
                TotalPoints += 1;
                CharCodeOfPreviousElement = CharCodeOfCurrentElement;
            }
        } 
    }
    if (IsCharASCII(CharCodeOfPreviousElement)) {
        TotalPoints += 3;

    } else if (IsNumberASCII(CharCodeOfPreviousElement)) {
        TotalPoints += 2;
    } else {
        TotalPoints += 1;
    }

    return TotalPoints;

}

module.exports = { Player, CalculateScoreWithRegEx, CalculateScoreWithoutRegEx }