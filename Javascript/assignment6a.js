"use strict"

// Interest rate a bank provides for general public. Slab1 is for below 6 months of duration. 
// Slab2 is for 6-12 months duration and 8 is for anything 1 year and above 
let GeneralRate = {
    "Slab1" : 6,
    "Slab2" : 7,
    "Slab3" : 8
};

// Interest rate a bank provides for senior citizens. Slab1 is for below 6 months of duration. 
// Slab2 is for 6-12 months duration and 8 is for anything 1 year and above 
let SeniorRate = {
    "Slab1" : 7,
    "Slab2" : 8,
    "Slab3" : 9
};

/*
Summary.
This function returns the interst rate based on the duration a person is depositing the amount and type of person they are.

@param      { Number }      Duration        The number of months a person wants to deposit a ceratin amount
@return     { Number }                      The interest rate the person will get based on the duration and if they are a senior citizen. 
*/
function FindRateBasedOnDuration (Duration) {
    if (Duration < 6){
        return this.Slab1;
    } else if (Duration < 12){
        return this.Slab2;
    } else if (Duration >=12){
        return this.Slab3;
    }
}

/*
Summary.
This function calculates and prints the interest for a person if they are coosing the option of fixed 
deposit. The function takes 2 seconds to complete its execution.

@param      { Number }      Amount          The amount a person wants to deposit as a fixed deposit
@param      { Number }      Duration        The number of months a person wants to deposit a ceratin amount
@return     { Promise } 
*/
function CalculateFixedDepositInterest (Amount, Duration) {
    return new Promise((Resolve, Reject) => {
        setTimeout(() => {
            var Rate, FindRateFunction, InterestAmount;
            FindRateFunction = FindRateBasedOnDuration.bind(this);
            Rate = FindRateFunction(Duration);
            InterestAmount = (Amount * Rate * (Duration / 12)) / 100;
            console.log("Interest Amount : " + InterestAmount);
            Resolve();
        }, 2000);
    });
}

/*
Summary.
This function calculates and prints the interest for a person if they are coosing the option of recurring 
deposit. The function takes 2 seconds to complete its execution.

@param      { Number }      Amount          The amount a person wants to deposit as a recurring deposit
@param      { Number }      Duration        The number of months a person wants to deposit a ceratin amount
@return     { Promise } 
*/
function CalculateRecurringDepositInterest (Amount, Duration) {
    return new Promise((Resolve, Reject) => {
        setTimeout(() => {
            var Rate, FindRateFunction, InterestAmount, Months;
            FindRateFunction = FindRateBasedOnDuration.bind(this);
            Rate = FindRateFunction(Duration);
            Amount = Amount / Duration;
            InterestAmount = 0;
            for (Months = 0; Months < Duration; Months++){
                InterestAmount += (Amount * Rate * ((Duration - Months) / 12)) / 100;
            }
            console.log("Interest Amount : " + InterestAmount);
            Resolve();
        }, 2000);
    });
};

/* 
Summary.
An asyncronous function which first calculates the fixed and recurring deposit interests 
for a general citizen and does the same for a senior citizen. 
*/
async function Main() {
    console.log("\nCalculating fixed depsit interest for general citizen");
    await CalculateFixedDepositInterest.apply(GeneralRate, [1000, 10]);
    console.log("\nCalculating recurring depsit interest for general citizen");
    await CalculateRecurringDepositInterest.apply(GeneralRate, [1000, 10]);
    console.log("\nCalculating fixed depsit interest for senior citizen");
    await CalculateFixedDepositInterest.call(SeniorRate, 1000, 10);
    console.log("\nCalculating recurring depsit interest for senior citizen");
    await CalculateRecurringDepositInterest.call(SeniorRate, 1000, 10);
}

Main();