"use strict"

// This is the set of addresses that needs to be sorted and added to the respective queues
const Addresses = [
    "150 E San Fernando St, San Jose, CA 95112",
    "525 W Santa Clara St, San Jose, CA 95113",
    "500 El Camino Real, Santa Clara, CA 95053",
    "200 E Santa Clara St 18th floor, San Jose, CA 95113",
    "101 E San Fernando St #100, San Jose, CA 95112",
    "322 E Santa Clara St #1909, San Joe, CA 95112",
    "1001 Railroad Ave, Santa Clara, CA asdln",
    {
        "Street" : "2039 El Camino Real",
        "City" : "Santa Clara", 
        "State" : "CA",
        "Pincode" :  95050
    },
    {
        "Street" : "2213 El Camino Real",
        "City" : "Santa Clara", 
        "State" : "CA",
        "Pincode" :  95050
    },
    {
        "Street" : "2004 El Camino Real",
        "City" : "Sata Clara", 
        "State" : "CA",
        "Pincode" :  95050
    },
    {
        "Street" : "550 Newhall Dr",
        "City" : "San Jose", 
        "State" : "CA",
        "Pincode" :  "qwerty"
    }
]

// This a the queue which will hold all the addresses that needs to be delivered in San Jose
var SanJoseQueue = []
// This a the queue which will hold all the addresses that needs to be delivered in Santa Clara
var SantaClaraQueue = []

/*
Summary.
This function removes all leading and trailing spaces from each element of the array.

@param      { Array }    Address     Delivery address stored as an array.
@return     { Array }    Address     Delivery address stored as an array.
*/
var RemoveSpacesInArray = (Address) => {   
    var value; 
    for (value in Address) {
        Address[value] = Address[value].trim();
    }
    return Address;
}

/*
Summary.
This function converts the delivery address stored as a string into an array by spliting the string at each comma.

@param      { String }    Address          Delivery address stored as a string.
@return     { Array }     AddressArray     Delivery address stored as an array.
*/
var ConvertStringToArray = (Address) => {
    var AddressArray, StateAndPinCode;
    AddressArray = Address.split(",");
    AddressArray = RemoveSpacesInArray(AddressArray);
    StateAndPinCode = AddressArray.pop().split(" ");
    StateAndPinCode[1] = parseInt(StateAndPinCode[1]);
    AddressArray = AddressArray.concat(StateAndPinCode);
    return AddressArray;
}

/*
Summary.
This function validates the address by checking the city name and if pincode exists. It also prints appropriate messages on finding errors.

@param      { String }    FormattedAddress       Delivery address in JSON format converted to a string.
@return     { Boolean }                          Returns true if the address has no errors, false if any errors are found
*/
var IsValidPincodeAndCity = (FormattedAddress) => {
    var FormattedAddress, Pincode, City;
    FormattedAddress = JSON.parse(FormattedAddress);
    Pincode = FormattedAddress["Pincode"];
    City = FormattedAddress["City"];
    if (!Number.isInteger(Pincode)){
        console.log("Pincode not found in ", JSON.stringify(FormattedAddress));
        return false;
    }

    if (!City.includes("San Jose") && !City.includes("Santa Clara")) {
        console.log("This service does not deliver to", City);
        return false;
    }
    return true;
}

/*
Summary.
This function converts delivery address stored as an array into JSON with 4 attributes (Street, City, State, Pincode).

@param      { Array }    AddressArray       Delivery address as an array.
@return     { JSON }     AddressJSON        All the elements of array stored in JSON.
*/
var AddValuesToJSON = (AddressArray) => {
    var AddressJSON = {};
    AddressJSON["Street"] = AddressArray.slice(0, AddressArray.length - 3).join(",");
    AddressJSON["City"] = AddressArray[AddressArray.length - 3];
    AddressJSON["State"] = AddressArray[AddressArray.length - 2];
    AddressJSON["Pincode"] = AddressArray[AddressArray.length - 1];
    return AddressJSON;
}

/*
Summary.
This function converts all the inputs given into a common format which is JSON.

@param      { String, JSON }    Address            Delivery address in string format or JSON.
@return     { String }          AddressJSON        Delivery address stored in JSON format converted to string.
*/
var FormatAddress = (Address) => {
    var AddressJSON, AddressArray;
    if (typeof(Address) == "string") {
        try {
            AddressJSON = JSON.parse(Address);
        }catch(ErrorMsg){
            AddressArray = ConvertStringToArray(Address);
            AddressJSON = AddValuesToJSON(AddressArray);
        }
    } else {
        AddressJSON = Address;
    }
    return JSON.stringify(AddressJSON);
}

/*
Summary.
This function takes in only the formatted address and adds them to the corresponding queue based on the city after checking if the address is deliverable and if a pincode is provided.

@param      { JSON }        FormattedAddress            Formatted delivery address which has no errors.
*/
var AddPackageToQueue = (FormattedAddress) => {
    if (IsValidPincodeAndCity(FormattedAddress)){
        FormattedAddress = JSON.parse(FormattedAddress);
        if (FormattedAddress["City"] == "San Jose") {
            SanJoseQueue.push(FormattedAddress);
        } else if (FormattedAddress["City"] == "Santa Clara") {
            SantaClaraQueue.push(FormattedAddress);
        }
    }

}

/*
Summary.
This function processes all addresses, formats them into common format and adds the address into it's corresponding queue. This function has a timeout of 5000ms.

@param      { Array }        AllAddresses       Set of all delivery addresses in which each element can be of string or JSON format.
@return     { Promise }
*/
var FormatAndAddAddressesToQueue = (AllAddresses) => {    
    var Address, FormattedAddress;
    return new Promise((resolve, reject)=>{        
        setTimeout(()=>{
            for (Address in AllAddresses){
                FormattedAddress = FormatAddress(AllAddresses[Address]);
                AddPackageToQueue(FormattedAddress);
            }
            resolve();
        }, 5000);        
    });
}

/*
Summary.
This function prints all the addresses present in a queue. This function has a timeout of 2500ms.

@param      { Array }        Queue       Set of all delivery addresses which is present in a specific queue for one of the cities.
@return     { Promise }
*/
var PrintQueueValues = (Queue) => {
    var value;
    return new Promise((resolve, reject)=>{        
        setTimeout(()=>{
            for (value in Queue) {
                console.log(Queue[value]);
            }
            resolve();
        }, 2500);        
    });

}

/*
Summary.
This is an asyncronous function which first formats and adds address to it's corresponding queues, later it prints the values present in each of the queues.
*/
async function main() {    
    console.log("Formatting addresses\n");
    await FormatAndAddAddressesToQueue(Addresses);
    console.log("\n\nPrinting Santa Clara Queue Values\n"); 
    await PrintQueueValues(SantaClaraQueue);
    console.log("\n\nPrinting San Jose Queue Values\n");
    await PrintQueueValues(SanJoseQueue);
}

main();