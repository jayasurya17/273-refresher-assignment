"use strict"

// These are the set of addresses that needs to be delivered to Santa Clara
let SantaClaraPackages = [{
    "name" : "Package 1",
    "street" : "The Alameda",    
    "city" : "Santa Clara",    
    "state" : "California"
},{
    "name" : "Package 2",
    "street" : "E San Fernando St",    
    "city" : "Santa Clara",    
    "state" : "California"
},{
    "name" : "Package 3",
    "street" : "S Market St",    
    "city" : "Santa Clara",    
    "state" : "California"
},{
    "name" : "Package 4",
    "street" : "Lincoln St",    
    "city" : "Santa Clara",    
    "state" : "California"
},{
    "name" : "Package 5",
    "street" : "550 Newhall Dr",
    "city" : "Santa Clara", 
    "state" : "California"
},{
    "name" : "Package 6",
    "street" : "Civic Center Dr",
    "city" : "Santa Clara", 
    "state" : "California"
}
];

// These are the set of addresses that needs to be delivered to San Jose
let SanJosePackages = [{
        "name" : "Package 7",
        "street" : "The Alameda",    
        "city" : "San Jose",    
        "state" : "California"
    },{
        "name" : "Package 8",
        "street" : "E San Fernando St",    
        "city" : "San Jose",    
        "state" : "California"
    },{
        "name" : "Package 9",
        "street" : "S Market St",    
        "city" : "San Jose",    
        "state" : "California"
    },{
        "name" : "Package 10",
        "street" : "Lincoln St",    
        "city" : "San Jose",    
        "state" : "California"
    },{
        "name" : "Package 11",
        "street" : "550 Newhall Dr",
        "city" : "San Jose", 
        "state" : "California"
    }
];
// These are the cabs which are available
var CabsAvailable = ["San Jose", "Santa Clara"]
// This is the list of all packages that have been sent
var AllPackagesSent = []

/*
Summary.
This function prints the package loaded and list of all packages sent.

@param      { String }    City                  City in which the package has to be delivered.
@param      { Array }     Names                 Set of packages that have been loaded onto the cab.
@return     { Function }  PrintPackageNames     Function which prints the package loaded and list of all packages sent
*/
var PackagesLoaded = (City, ...Names) => {
    AllPackagesSent = [...AllPackagesSent, ...Names]
    var PrintPackageNames = () => {
        console.log(JSON.stringify(Names), "has been loaded in ", City, "cab");
        console.log("All packages sent", JSON.stringify(AllPackagesSent), "\n");
    }
    return PrintPackageNames;
}

/*
Summary.
This function returns the name of the package from the address JSON.

@param      { JSON }    Package     Name and delivery address of the package.
@return     { String }  Name        Name of the package.
*/
var GetPackageName = (Package) => {
    let { name } = Package;
    return name;
}

/*
Summary.
This function adds cab back to the queue once the package is delivered. San Jose cab takes 2 seconds to return back and Santa Clara cab takes 3 seconds to return back.

@param      { String }    City     Name of the city the package has to be delivered to.
*/
var AddCabBackToQueue = (City) => {
    var TimeTaken;
    if (!City.localeCompare("San Jose")) {
        TimeTaken = 2000;
    } else if (!City.localeCompare("Santa Clara")){
        TimeTaken = 3000;
    }
    setTimeout(()=>{
        CabsAvailable.push(City);
    }, TimeTaken);
}

/*
Summary.
This function loads the first two 2 packages onto the cab and sends it. If there is only 1 package available, then only package is loaded and sent.  

@param      { String }    NameOfCity        Name of the city the package has to be delivered to.
@param      { Array }     ListOfPackages    List of packages that has to be delivered to a city.
@return     { Function }  PackagesSent      Function which prints the package loaded and list of all packages sent.
*/
var SendPackages = (NameOfCity, ListOfPackages) => {
    var PackagesSent;
    if (ListOfPackages.length >= 2){
        let [Package1, Package2] = ListOfPackages;
        ListOfPackages.shift();
        ListOfPackages.shift();
        PackagesSent = PackagesLoaded(NameOfCity, GetPackageName(Package1), GetPackageName(Package2));                    
    } else if (ListOfPackages.length == 1) {
        let [Package1] = ListOfPackages;
        ListOfPackages.shift();
        PackagesSent = PackagesLoaded(NameOfCity, GetPackageName(Package1));
    }
    return PackagesSent;
}

/*
Summary.
This function takes 1 second to load the next cab in the queue. 

@return     { Promise }
*/
var LoadCab = () => {    
    return new Promise((Resolve, Reject) => {
        setTimeout(()=>{
            if (CabsAvailable.length == 0){
                console.log("Waiting for next cab\n");
                Resolve();
                return;
            }        
            var City = CabsAvailable.shift();
            var PackagesSent;
            if (City == "San Jose"){
                PackagesSent = SendPackages(City, SanJosePackages);
                PackagesSent(); 
            } else if (City == "Santa Clara"){
                PackagesSent = SendPackages(City, SantaClaraPackages);
                PackagesSent(); 
            } 
            AddCabBackToQueue(City);
            Resolve();
        }, 1000);
    });
};

/*
Summary.
This is an asyncronous function which keeps loading the cab until all the packages are sent.
*/
async function main() {
    while (SanJosePackages.length != 0 || SantaClaraPackages != 0) {
        await LoadCab();
    }
    console.log("Waiting for all cabs to return back to queue!");
    console.log("All packages shipped!");
}

main();