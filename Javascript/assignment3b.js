"use strict"
var CalculateScore = require("./assignment3a.js")

var Player1 = new CalculateScore.Player("Javascript");
CalculateScore.Player.SetValue.call(Player1, "AAbb11C2.")
console.log("Score calculated with regular expression : " + CalculateScore.CalculateScoreWithRegEx(CalculateScore.Player.GetValue.call(Player1)));
console.log("Score calculated without regular expression : " + CalculateScore.CalculateScoreWithoutRegEx(CalculateScore.Player.GetValue.call(Player1)));