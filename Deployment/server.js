"use strict"
const express = require('express');
const app = express();
const fetch = require("node-fetch");
const bodyParser = require('body-parser');

app.set('view engine', 'jade');
app.set('views','./views');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(bodyParser.urlencoded({ extended: true }));

/*
Summary.
This class fetches information, stores and returns the value when needed.
*/
class FetchInfo {
    constructor() {
        this.FetchedResults = [];
    }
    SetValue(Value) {
        this.FetchedResults = [...this.FetchedResults, ...Value];
    }

    GetValue() {
        return this.FetchedResults;
    }

    NumberOfPagesToBeFetched(Count) {
        // console.log(Count, typeof(Count), parseInt(Count), Math.ceil((parseFloat(Count)) / 10))
        return (Math.ceil((parseFloat(Count)) / 10));
    }

    async FetchAndPrint(FormData) { 
        var FetchedJSONValue, count;
        // console.log(this.NumberOfPagesToBeFetched(FormData.numberOfRecipes));
        for (var count = 0; count < this.NumberOfPagesToBeFetched(FormData.numberOfRecipes); count++){
            await fetch(URLToFetchData(FormData) + "&p=" + (count + 1).toString()).then(function(ResponseFromURL) {
                return ResponseFromURL.json();
            }).then(function(JSONFromURL) {
                FetchedJSONValue = JSONFromURL;
            });
            if (FetchedJSONValue['results'].length == 0){
                break
            }
            this.SetValue(FetchedJSONValue['results']);
        }
    }
}

/*
Summary.
This function returns the URL from which required information has to be fetched.

@param      { JSON }        FormData    Data received from user input given in the form
@return     { String }      URL         The complete URL from which information can be fetched
*/
var URLToFetchData = (FormData) => {
    var URL = "http://www.recipepuppy.com/api/?";
    let  { ingredients, mealName } = FormData
    URL = URL + "i=" + ingredients + "&q=" + mealName;
    // console.log(URL);
    return URL;
}


// Route '/' with get request
app.get('/', function(req, res){
    res.render('index');
    // console.log("ASdasd");
});


// Route '/' with post request
app.post('/', function(req, res){
    var Recipes = new FetchInfo();
    // var FetchedData = {}
    // console.log(req.body);
    Promise.resolve(Recipes.FetchAndPrint(req.body)).then(function(){
        res.render('printValues', { 
            JSONData : Recipes.GetValue(), 
            FormValues : req.body
        });
    });
});

app.listen(3000);