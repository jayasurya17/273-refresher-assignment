"use strict"


//List of video names along with the genres they belong to
var VideoNames = {
    "Video 1" : ["Comedy", "Action"],
    "Video 2" : ["Comedy", "Horror"],
    "Video 3" : ["Mystery", "Thriller", "Drama"],
    "Video 4" : ["Philosophical"],
    "Video 5" : ["Romance", "Drama"],
    "Video 6" : ["Crime", "Adventure"],
    "Video 7" : ["Horror"],
    "Video 8" : ["Romance"],
    "Video 9" : ["Mystery", "Horror"],
    "Video 10" : ["Action", "Adventure"],
}

/*
Summary.
This function returns the username which is stored in the session at any given point of time.

@return     { String }      Username which is present in the session storage 
*/
function GetUserNameInSession() {
    return sessionStorage.getItem("VideoSuggestorUsername");
}

/*
Summary.
This function checks if any username is present in the session storage at any given point of time.

@return     { Boolean }     Returns true if a username is found. Returns false if no username is found.
*/
function IsUsernameSetInSessionStorage() {
    var VideoSuggestorUsername;
    VideoSuggestorUsername = sessionStorage.getItem("VideoSuggestorUsername");
    if(VideoSuggestorUsername == null) {
        return false;
    }
    return true;
} 

/*
Summary.
This function checks if a session storage is present. It creates a session storage and stores username 
in it if no session storage is found. Removes the session storage if it is found. It also enables and 
disables the input box while modifying the text shwon to the user. It displays relavent video 
suggestions after performing the above activities.
*/
function StoreOrRemoveSessionUsername() {
    var VideoSuggestorUsernameElement, UsernameSubmitButtonElement;
    VideoSuggestorUsernameElement = document.getElementById("video-suggestor-username");
    UsernameSubmitButtonElement = document.getElementById("submit-form-button");
    if (IsUsernameSetInSessionStorage()){
        VideoSuggestorUsernameElement.disabled = false;
        UsernameSubmitButtonElement.value = "Show videos";
        sessionStorage.removeItem("VideoSuggestorUsername");
    } else {
        sessionStorage.setItem("VideoSuggestorUsername", VideoSuggestorUsernameElement.value);
        VideoSuggestorUsernameElement.disabled = true;
        UsernameSubmitButtonElement.value = "Change username";
    }
    DisplayVideoSuggestions();
}

/*
Summary.
This function changes the style of the cursor to pointer.

@param      { HTML element }    Value       HTML element for which the cursor icon has to be seen as pointer. 
*/
function ChangeCursorToPointer(Value) {
    Value.style.cursor = "pointer";
}

/*
Summary.
This function returns the JSON value from local storage which has the number of times each user 
has watched every genre of video. If there is no record of the user then the function returns an empty JSON.

@param      { JSON }    VideoSuggestorHistory       JSON value from local storage which has the number of 
                                                    times each user has watched every genre of video.
*/
function HistoryValue() {
    var LocalStorageValue, VideoSuggestorHistory;
    LocalStorageValue = localStorage.getItem("VideoSuggestionsHistory");
    if (LocalStorageValue == null) {
        VideoSuggestorHistory = {}
    } else {
        VideoSuggestorHistory = JSON.parse(LocalStorageValue);
    }
    return VideoSuggestorHistory;

}

/*
Summary.
This function increases the count for number of times a genre has been watched by a user based on
the current video selected. If no record exists for the user then a new record is created. This 
value is updated and stored in the local storage of the browser.

@param      { JSON }    History     JSON value from local storage which has the number of times 
                                    each user has watched every genre of video.
@param      { String }  VideoName   Name of the video which the user has selected to watch
*/
function AddValueToHistory(History, VideoName) {
    var tag, UserNameInSession;
    UserNameInSession = GetUserNameInSession();
    if (History[UserNameInSession] == null) {
        History[UserNameInSession] = {};
        for (tag in VideoNames[VideoName]){
            History[UserNameInSession][VideoNames[VideoName][tag]] = 1;
        }
    } else {
        for (tag in VideoNames[VideoName]){
            if (History[UserNameInSession][VideoNames[VideoName][tag]] == null) {
                History[UserNameInSession][VideoNames[VideoName][tag]] = 1;
            } else {                            
                History[UserNameInSession][VideoNames[VideoName][tag]] += 1;
            }
        }
    }
    localStorage.setItem("VideoSuggestionsHistory", JSON.stringify(History));
}

/*
Summary.
This function checks if the current user has any record stored in the local storage.

@return      { Boolean }     True if thea record is found. False if no record is found.
*/
function DoesUserHaveHistory() {
    var UserNameInSession = GetUserNameInSession();
    var History = JSON.parse(localStorage.getItem("VideoSuggestionsHistory"));
    if (History == null) {
        return false;
    } else if (History[UserNameInSession] == null) {
        return false;
    }
    return true;
}

/*
Summary.
This function updates the history stored in the local storage and plays the 
currently selected video. Recommendations are also updated based on the selection.

@param      { String }      VideoElement    Name of the video that a user wanted to watch.
*/
function UpdateHistoryAndPlayVideo(VideoElement) {
    var VideoSuggestorHistory;
    VideoSuggestorHistory = HistoryValue();
    AddValueToHistory(VideoSuggestorHistory, VideoElement.innerHTML);
    PlayVideo(VideoElement.innerHTML);
    UpdateRecommendations(); 
}

/*
Summary.
This function updates the video player element.
*/
function PlayVideo(VideoName) {
    document.getElementById("play-video-container-text").innerHTML = "Playing video " + VideoName;
}

/*
Summary.
This function finds the most popular genre for a particular username based on the values 
stored in history which is stored in the local storage.

@return     { Number }      MaxGenre      The genre which is most popular for the user.
*/
function MostPopularVideoGenreForUser() {
    var UserNameInSession, val;
    UserNameInSession = GetUserNameInSession();
    var VideoSuggestorHistory = HistoryValue();
    var MaxVal = 0, MaxGenre = "";
    for (val in VideoSuggestorHistory[UserNameInSession]){
        if (VideoSuggestorHistory[UserNameInSession][val] > MaxVal){
            MaxVal = VideoSuggestorHistory[UserNameInSession][val];
            MaxGenre = val;
        }
    }
    return MaxGenre;
}

/*
Summary.
This function populates the recommendations table based on the most popular genre of video of the 
active user. On selecting each video the history for the user has to be updated in the local storage.

@param      { HTML element }    Table   Table element where the video recommendations has to be populated
*/
function PopulateRecommendedVideoNames(Table) {                
    var VideoTag = MostPopularVideoGenreForUser();   

    var VideoNameRow, VideoNameDesc, value;
    VideoNameRow = document.createElement("tr");
    Table.appendChild(VideoNameRow);
    VideoNameDesc = document.createElement("td");
    VideoNameDesc.innerHTML = "Video Name";
    VideoNameRow.appendChild(VideoNameDesc);
    VideoNameDesc = document.createElement("td");
    VideoNameDesc.innerHTML = "Genres";
    VideoNameRow.appendChild(VideoNameDesc);             
    for (value in VideoNames) {
        if (VideoNames[value].includes(VideoTag)) {
            VideoNameRow = document.createElement("tr");
            Table.appendChild(VideoNameRow);
            VideoNameDesc = document.createElement("td");
            VideoNameDesc.innerHTML = value;
            VideoNameDesc.onclick = function () { UpdateHistoryAndPlayVideo(this) };
            VideoNameDesc.onmouseover = function () { ChangeCursorToPointer(this) };
            VideoNameRow.appendChild(VideoNameDesc);
            VideoNameDesc = document.createElement("td");
            VideoNameDesc.innerHTML = VideoNames[value];
            VideoNameRow.appendChild(VideoNameDesc);
        }
    } 
}

/*
Summary.
This function clears the recommendation table.

@param      { HTML element }    Table   Table element where the video recommendations has to be populated
*/
function ClearRecommendations(Table) {
    while (Table.firstChild) {
        Table.removeChild(Table.firstChild);
    }
}

/*
Summary.
This function updates the recommendation table. It first clears the table and then populates the table is 
the username has any history.
*/
function UpdateRecommendations() {
    var RecommendedVideosContainer = document.getElementById("recommended-videos-container");
    var RecommendedVideosTable = document.getElementById("recommended-videos-table");
    ClearRecommendations(RecommendedVideosTable);
    if (DoesUserHaveHistory()) {
        RecommendedVideosContainer.style.display = "block";
        PopulateRecommendedVideoNames(RecommendedVideosTable);
    } else {
        RecommendedVideosContainer.style.display = "none";
    }
}

/*
Summary.
This function displays all the videos along with the recommendations if a user is active. Else it displays appropriate message.
*/
function DisplayVideoSuggestions() {
    var VideoSuggestorUsernameElement, VideoNamesContainer, UsernameSubmitButtonElement, Container, value;
    VideoNamesContainer = document.getElementById("video-names-container");
    UsernameSubmitButtonElement = document.getElementById("submit-form-button");
    VideoNamesContainer.innerHTML = "";
    if (IsUsernameSetInSessionStorage()) {
        UsernameSubmitButtonElement.value = "Change username";
        VideoSuggestorUsernameElement = document.getElementById("video-suggestor-username");
        VideoSuggestorUsernameElement.value = GetUserNameInSession();
        VideoSuggestorUsernameElement.disabled = "true";
        var VideoNameTable, VideoNameRow, VideoNameDesc, Instructions;


        //Recommendations
        Container = document.createElement("div");
        Container.id = "recommended-videos-container";
        VideoNamesContainer.appendChild(Container);
        Instructions = document.createElement("h3");
        Instructions.innerHTML = "Recommended for " + GetUserNameInSession() + " based on history";
        Container.appendChild(Instructions);
        VideoNameTable = document.createElement("table");
        VideoNameTable.id = "recommended-videos-table";
        Container.appendChild(VideoNameTable);
        UpdateRecommendations();

        //All videos
        Instructions = document.createElement("h3");
        Instructions.innerHTML = "All videos";
        VideoNamesContainer.appendChild(Instructions);
        VideoNameTable = document.createElement("table");
        VideoNameTable.id = "all-videos-table";
        VideoNamesContainer.appendChild(VideoNameTable);
        VideoNameRow = document.createElement("tr");
        VideoNameTable.appendChild(VideoNameRow);
        VideoNameDesc = document.createElement("td");
        VideoNameDesc.innerHTML = "Video Name";
        VideoNameRow.appendChild(VideoNameDesc);
        VideoNameDesc = document.createElement("td");
        VideoNameDesc.innerHTML = "Genres";
        VideoNameRow.appendChild(VideoNameDesc);
        for (value in VideoNames) {
            VideoNameRow = document.createElement("tr");
            VideoNameTable.appendChild(VideoNameRow);
            VideoNameDesc = document.createElement("td");
            VideoNameDesc.innerHTML = value;
            VideoNameDesc.onclick = function () { UpdateHistoryAndPlayVideo(this) };
            VideoNameDesc.onmouseover = function () { ChangeCursorToPointer(this) };
            VideoNameRow.appendChild(VideoNameDesc);
            VideoNameDesc = document.createElement("td");
            VideoNameDesc.innerHTML = VideoNames[value];
            VideoNameRow.appendChild(VideoNameDesc);
        }                    
    } else {
        UsernameSubmitButtonElement.value = "Show videos";
        VideoNamesContainer.innerHTML = "Please enter a username to suggest videos";
    }
}