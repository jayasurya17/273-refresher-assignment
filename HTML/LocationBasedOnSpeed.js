"use strict"

// Variable which holds the ID value of the interval timer that is set
var RepeatEvery10ms;

/*
Summary.
This function changes the style of the cursor to pointer.

@param      { HTML element }    Value       HTML element for which the cursor icon has to be seen as pointer. 
*/
function PointerCursor(Value) {
    Value.style.cursor = "pointer";
}

function GetCurrentLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(DisplayValues);
    } else {
        alert ("Sorry! The browser you are using does not support geolocation");
    }
}

/*
Summary.
This function displays the latitude and longitude values on the screen.

@param      { Geolocation }    CurrentPosition       Current GPS position of the user. 
*/
function DisplayValues(CurrentPosition) {
    document.getElementById("latitude-value").innerHTML = CurrentPosition.coords.latitude;
    document.getElementById("longitude-value").innerHTML = CurrentPosition.coords.longitude;
}

/*
Summary.
This function changes the latitude value based on the speed the user is going.

@param      { Number }    Speed                     Speed the user has selected for them to be travelling. 
@return     { Float }     LatitudeChangePer10ms     The change in latitude the user will have in 10 ms based 
                                                    on the speed they are travelling.
*/
function ChangeInLatitude(Speed) {
    var LatitudeDegreePerKM, LatitudeChangePer10ms;
    LatitudeDegreePerKM = 1 / 111;
    LatitudeChangePer10ms = (LatitudeDegreePerKM * Speed) / 36000;
    return LatitudeChangePer10ms;
}

/*
Summary.
This function changes the longitude value based on the speed the user is going.

@param      { Number }    Speed                     Speed the user has selected for them to be travelling. 
@return     { Float }     LongitudeChangePer10ms    The change in longitude the user will have in 10 ms based 
                                                    on the speed they are travelling.
*/
function ChangeinLongitude(Speed) {
    var LongitudeDegreePerKM, LongitudeChangePer10ms, CurrentLatitude, KMPerDegreeofLongitude;
    CurrentLatitude = parseFloat(document.getElementById("latitude-value").innerHTML);
    KMPerDegreeofLongitude = (90 - CurrentLatitude) * 111;
    LongitudeDegreePerKM = 1 / KMPerDegreeofLongitude;
    LongitudeChangePer10ms = (LongitudeDegreePerKM * Speed) / 36000;
    return LongitudeChangePer10ms;
}

/*
Summary.
This function updates the longitude value based on the direction and a user is travelling. The value 
is updated every 10 ms. Longitude value decreases while travelling towards west and increases towards east.

@param      { String }    Direction       Direction the user has selected for them to be travelling. 
@param      { Number }    Speed           Speed the user has selected for them to be travelling. 
@return     { ID }                        ID value of the interval timer           
*/
function EastOrWestSpeed(Direction, Speed) {
    var CurrentLongitude, NewLongitude;
    return setInterval(() => {
        CurrentLongitude = document.getElementById("longitude-value").innerHTML;
        if (Direction.localeCompare("East") == 0){
            NewLongitude = parseFloat(CurrentLongitude) + ChangeinLongitude(Speed);
        } else if (Direction.localeCompare("West") == 0){
            NewLongitude = parseFloat(CurrentLongitude) + ChangeinLongitude(-Speed);
        }
        if (!isNaN(NewLongitude)){
            document.getElementById("longitude-value").innerHTML = NewLongitude;
            document.getElementById("speed-value").innerHTML = Speed + " kmph towards " + Direction;
        }
    }, 100);
}

/*
Summary.
This function updates the latitude value based on the direction and a user is travelling. The value 
is updated every 10 ms. latitude value decreases while travelling towards south and increases towards north.

@param      { String }    Direction       Direction the user has selected for them to be travelling. 
@param      { Number }    Speed           Speed the user has selected for them to be travelling. 
@return     { ID }                        ID value of the interval timer           
*/
function NorthOrSouthSpeed(Direction, Speed) {
    var CurrentLatitude, NewLatitude;
    return setInterval(() => {
        CurrentLatitude = document.getElementById("latitude-value").innerHTML;
        if (Direction.localeCompare("North") == 0){
            NewLatitude = parseFloat(CurrentLatitude) + ChangeInLatitude(Speed);
        } else if (Direction.localeCompare("South") == 0){
            NewLatitude = parseFloat(CurrentLatitude) + ChangeInLatitude(-Speed);
        }
        if (!isNaN(NewLatitude)){
            document.getElementById("latitude-value").innerHTML = NewLatitude;
            document.getElementById("speed-value").innerHTML = Speed + " kmph towards " + Direction;
        }
    }, 100);
}

/*
Summary.
This function clears the ID value of the interval timer. It also updates the text displayed.
*/
function ClearIntervalAndValues() {
    clearInterval(RepeatEvery10ms);
    document.getElementById("speed-value").innerHTML = "0 kmph";
}